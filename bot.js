console.log('Twibot is starting...');

var twit = require('twit');
var readline = require('readline');
var token = require('./token');

var Twitter = new twit(token);

var ReadLine = readline.createInterface({
	input: process.stdin
,	output: process.stdout
});

console.log("Twibot used 'token.json' and successfully connected to Twitter...");

ReadLine.question('Do you want to [p]ost or to [s]earch? ', (selectedMode) => {

	ReadLine.close();

	if(selectedMode == "s") {

		var ReadLineSearch = readline.createInterface({
			input: process.stdin
		,	output: process.stdout
		});

		ReadLineSearch.question('What do you want to search? ', (searchQuerry) => {

			ReadLineSearch.close();

			Twitter.get('search/tweets', {"q": searchQuerry, "count": "10"}, showTweets);

			console.log('Searching Tweets...')

			function showTweets(err, data, responce) {

				var tweets = data.statuses;

				for(var i=0; i<tweets.length; i++) {
					
					console.log();
					console.log("----------- Tweet -----------");
					console.log(tweets[i].text);
					console.log("--------- End Tweet ---------");
				}
			}
		});
	} else if(selectedMode == "p") {

		var ReadLinePost = readline.createInterface({
			input: process.stdin
		,	output: process.stdout
		});

		ReadLinePost.question('What do you want to post? ', (tweetToPost) => {
			
			ReadLinePost.close();

			Twitter.post('statuses/update', {status: tweetToPost}, Tweet);

			function Tweet(err, data, responce) { console.log("Successfully posted provided text!"); }
		});

	} else {

		console.log("No valid answer given. Killing the bot...")
	}
});